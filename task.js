// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi, 
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';

// class vehicle
class Vehicle {
    constructor(jenisKendaraan,negaraProduksi){
        this.jenisKendaraan = jenisKendaraan;
        this.negaraProduksi = negaraProduksi;
    }
    // Method
    info(){
        return(`Jenis Kendaraan ini ${this.jenisKendaraan} dari Negara ${this.negaraProduksi}`);
    }
}
// instance
const vehicle = new Vehicle("Roda 2", "Jepang");
const vehicle2 = new Vehicle("Roda 4", "Jerman");
console.log(vehicle.info());
console.log(vehicle2.info());

// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
// 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
// 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
// print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'

// class child from vehicle
class Car extends Vehicle {
    // constructor
    constructor(jenisKendaraan, negaraProduksi, merek, harga, persentasePajak) {
      // parent class constructor
      super(jenisKendaraan, negaraProduksi);
      // property in child class
      this.merek = merek;
      this.harga = harga;
      this.persentasePajak = persentasePajak;
    }
    //  instance method
    totalHarga() {
      return this.harga + this.harga * (100 / this.persentasePajak);
    }
    //   override info metod
    info() {
      //  parent method
      super.info();
      console.log(`Kendaraan ini mereknya ${this.merek} dengan total harga Rp. ${this.totalHarga()}`);
    }
  }


// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil)) 
const kendaraan = new Vehicle(2, "Jepang");
kendaraan.info();

const bmw = new Car(4, "Jerman", "Audi",80000000, 10);
bmw.info();

// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()
// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/
